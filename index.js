window.onload = function()
{
    var canvas = document.getElementById('jeu');
        if(!canvas)
        {
            alert("Impossible de récupérer le canvas");
            return;
        }
    var context = canvas.getContext('2d');
        if(!context)
        {
            alert("Impossible de récupérer le context du canvas");
            return;
        }
    // code du jeu
    
    
    var snake = [];
    var scores = []; // on initialise le tableau des résultats
    var direction = 2; // 1 : top; 2 : right; 3 : bottom; 4 : left;
    var length_min = 12;
    var speed_snake = 100; //rafraichissement toutes les 100ms
    var canvas_width = document.querySelector("#jeu").width;
    var canvas_height = document.querySelector("#jeu").height;
    var fruit = null; // un fruit est toujours présent du le terrain de jeu
    var nb_fruit = 0; // nombre de fruits apparus durant la partie
    var nb_fruit_get = 0; //nombre de fruits gobés durant la partie
    var exp = null; // timer d'explosion d'un fruit
    var bonus_active = null; // si un bonus est présent sur le terrain
    var nb_bonus = 0; // nombre de bonus apparu dans le jeu
    var nb_bonus_get = 0; // nombre de bonus gobés dans le jeu
    var interval_bonus = 5; //nombre de fruits nécessaires pour l'apparition d'un bonus
    var color = '#42faa1'; // couleur de départ du bonus
    var rad = 20; // rayon du bonus
    var bon = null; // timer du bonus
    var points = 0; // on initialise les points à 0
    var pause = 0; //0 -> le jeu est en cours / 1 -> le jeu est en pause
    var direction_blocked = false; //true : le serpent est en cours de déplacement / pour éviter les saisies multiples pendant un interval
    var log_key = "keys : "; // stocke les touches préssées
    var game_lose = false; // true lorsque la partie est perdue
    var temps_jeu = null; // timer pour compter les secondes écoulées
    var time_second = 0; // le nombre de secondes écoulées depuis le début de la partie
    var jeu = null; // timer du jeu
    var menu_page = false; // true si l'on se trouve à la page de menu
    var scores_page = false; // true si l'on se trouve à la page des scores
    var request_send_score = false; // la requete de récupération des scores n'est pas en cours d'envoi
    
    
    /////////////////////////////// Classe des éléments du serpent
    
    class Item {  //snake's items
      
        constructor(posx, posy) {
            this.posx = posx; // ces coordonnées correspondent au coin Nord-Ouest de l'élément
            this.posy = posy;
        }
        
        draw() {
            context.fillStyle = "rgba(72, 137, 87, 0.5)";
            context.fillRect(this.posx, this.posy, 20, 20);
            context.lineWidth = 1;
            context.strokeStyle = "rgba(0,0,0, 0.8)";
            context.strokeRect(this.posx, this.posy, 20, 20);        
        
        }
        
        erase() {
            context.clearRect(this.posx -1, this.posy -1, 22, 22);
        }
        
        draw_head() {
            if (direction == 3) { //down
                context.fillStyle = "rgba(0, 0, 0, 1)";
                context.fillRect(this.posx + 3, this.posy + 13, 4, 4); // oeil
                context.fillStyle = "rgba(0, 0, 0, 1)";
                context.fillRect(this.posx + 13, this.posy + 13, 4, 4); // oeil
                context.fillStyle = "rgba(250, 0, 0, 1)";
                context.fillRect(this.posx + 8, this.posy + 20, 4, 10); // langue
            } else if (direction == 1) {  //up
                context.fillStyle = "rgba(0, 0, 0, 1)";
                context.fillRect(this.posx + 3, this.posy + 3, 4, 4);
                context.fillStyle = "rgba(0, 0, 0, 1)";
                context.fillRect(this.posx + 13, this.posy + 3, 4, 4);
                context.fillStyle = "rgba(250, 0, 0, 1)";
                context.fillRect(this.posx + 8, this.posy - 10, 4, 10);
            } else if (direction == 2) {  //right
                context.fillStyle = "rgba(0, 0, 0, 1)";
                context.fillRect(this.posx + 13, this.posy + 3, 4, 4);
                context.fillStyle = "rgba(0, 0, 0, 1)";
                context.fillRect(this.posx + 13, this.posy + 13, 4, 4);
                context.fillStyle = "rgba(250, 0, 0, 1)";
                context.fillRect(this.posx + 20, this.posy + 8, 10, 4);
            } else if (direction == 4) {  //left
                context.fillStyle = "rgba(0, 0, 0, 1)";
                context.fillRect(this.posx + 3, this.posy + 3, 4, 4);
                context.fillStyle = "rgba(0, 0, 0, 1)";
                context.fillRect(this.posx + 3, this.posy + 13, 4, 4);
                context.fillStyle = "rgba(250, 0, 0, 1)";
                context.fillRect(this.posx -10, this.posy + 8, 10, 4);
            }
        }
        
        erase_head() {
            context.clearRect(this.posx + 3, this.posy + 3, 4, 4);
            context.clearRect(this.posx + 3, this.posy + 13, 4, 4);
            context.clearRect(this.posx + 13, this.posy + 3, 4, 4);
            context.clearRect(this.posx + 13, this.posy + 13, 4, 4);
            context.clearRect(this.posx + 8, this.posy + 20, 4, 10);
            context.clearRect(this.posx + 8, this.posy - 10, 4, 10);
            context.clearRect(this.posx + 20, this.posy + 8, 10, 4);
            context.clearRect(this.posx -10, this.posy + 8, 10, 4);
        }
        
        
        
        get X() {
            return this.posx;
        }
        
        get Y() {
            return this.posy;
        }
    }
    
    ////////////////// Fonction sur le serpent ///////////////////////////////
    
    // Affiche le serpent en dessinant tous ses éléments
    function draw_snake() {
        snake[snake.length-2].erase_head(); // un nouvel item a été ajouté l'ancienne tête est donc à l'avant dernière case
        for (var i = 0; i < snake.length; i++) {
          snake[i].erase();
          snake[i].draw();    
        }
        snake[snake.length-1].draw_head();
        if (exp == null) {draw_fruit(fruit.X +10, fruit.Y +10);} //on redéssine le fruit au cas où il serait affecter par un erase item ou head, uniquement si le fruit n'est pas en cours d'explosion
        if (bonus_active != null) {bonus(bonus_active.X, bonus_active.Y, rad);}
    }
    
    // Ajoute un élément au serpent
    function add_snake(posx, posy, add_item = false) { //add_item vrai uniquement si le serpent s'allonge
        var item = new Item(posx, posy);      
        snake.push(item);
        if( (snake.length > length_min) && (!add_item)) {
            snake[0].erase();
            snake.shift();
        }
        //print_snake(); // pour le débugage
    }
    
    // Fait avancer le serpent en fonction de la direction courante
    function go_snake() {
        
        if (temps_jeu == null) { // si il s'agit du début de la partie on lance le chronomètre
            temps_jeu = setInterval(write_time,1000);
        }
        
        if ((bonus_active == null) && (nb_fruit >= (nb_bonus + 1) * interval_bonus + 1)) { // si l'on a dépassé le nombre de fruits nécessaires pour faire apparaitre un bonus
            var x_bonus = Math.floor(Math.random() * canvas_width/20)*20; 
            var y_bonus = Math.floor(Math.random() * canvas_width/20)*20;
            while(!is_free_bonus(x_bonus, y_bonus, rad)) {
                x_bonus = Math.floor(Math.random() * canvas_width/20)*20; 
                y_bonus = Math.floor(Math.random() * canvas_width/20)*20;
            }
            bonus_active = new Item(x_bonus, y_bonus);
            bon = setInterval(function() {erase_bonus(bonus_active.X, bonus_active.Y, rad + 1); bonus(bonus_active.X, bonus_active.Y, rad); rad = rad -1; draw_snake();}, 250);
            nb_bonus++;
        }
        
        log_key = log_key + "|"; // on ajoute un interval dans les logs
        switch (direction) {
            case 1 :  // up
                if (snake[snake.length-1].Y <= 0) { // le serpent atteint le limite haute du canvas
                    add_snake(snake[snake.length-1].X, canvas_height - 20);
                } else {
                    add_snake(snake[snake.length-1].X, snake[snake.length-1].Y - 20);
                }
                if (collision_tail()) { //le serpent se mord la queue
                  clearInterval(jeu);
                  clearInterval(temps_jeu);
                  game_over(); //animation de game over
                  play_loose();
                  game_lose = true;
                  if (bonus_active != null) {
                          clearInterval(bon);
                        }
                } else { 
                      if (collision_fruit()) {
                          exp = setInterval(function() { explosion(fruit.X + 10, fruit.Y + 10, 10)}, 50);
                          play_fruit();
                          length_min++;
                          points = points + 50; // on ajoute 50 points
                          document.querySelector("#points_value").textContent = points;
                          nb_fruit_get++;
                          document.querySelector("#fruits_value").textContent = nb_fruit_get;
                } else {
                    if (collision_bonus()) {
                        clearInterval(bon);
                        erase_bonus(bonus_active.X, bonus_active.Y, rad);
                        bonus_active = null;
                        play_bonus_get();
                        points = points + (rad - 5) * 20; // on ajoute un nombre de points au prorata de la taille du bonus
                        rad = 20;
                        document.querySelector("#points_value").textContent = points;
                        nb_bonus_get++;
                        document.querySelector("#bonus_value").textContent = nb_bonus_get;
                    }    
                }
                draw_snake(); 
                }
              break;  
            case 2 :  // right
                if (snake[snake.length-1].X >= canvas_width - 20) { // le serpent atteint le limite droite du canvas
                    add_snake(0, snake[snake.length-1].Y);
                } else {
                    add_snake(snake[snake.length-1].X + 20, snake[snake.length-1].Y);
                }
                if (collision_tail()) { //le serpent se mord la queue
                    clearInterval(jeu);
                    clearInterval(temps_jeu);
                    game_over(); //animation de game over
                    play_loose();
                    game_lose = true;                    
                    if (bonus_active != null) {
                          clearInterval(bon);
                        }
                  } else {
                    if (collision_fruit()) {
                          exp = setInterval(function() { explosion(fruit.X + 10, fruit.Y + 10, 10)}, 50);
                          play_fruit();
                          length_min++;
                          points = points + 50; // on ajoute 50 points
                          document.querySelector("#points_value").textContent = points;
                          nb_fruit_get++;
                          document.querySelector("#fruits_value").textContent = nb_fruit_get;
                    } else {
                        if (collision_bonus()) {
                            clearInterval(bon);
                            erase_bonus(bonus_active.X, bonus_active.Y, rad);
                            bonus_active = null;
                            play_bonus_get();
                            points = points + (rad - 5) * 20; // on ajoute un nombre de points au prorata de la taille du bonus
                            document.querySelector("#points_value").textContent = points;
                            nb_bonus_get++;
                            document.querySelector("#bonus_value").textContent = nb_bonus_get;
                            rad = 20;
                        }    
                    }
                    draw_snake();
                  }
                break;  
            case 3 :  // down
                 if (snake[snake.length-1].Y >= canvas_height - 20) { // le serpent atteint le limite basse du canvas
                    add_snake(snake[snake.length-1].X, 0);
                } else {
                    add_snake(snake[snake.length-1].X, snake[snake.length-1].Y + 20);
                }
                if (collision_tail()) { //le serpent se mord la queue
                    clearInterval(jeu);
                    clearInterval(temps_jeu);
                    game_over(); //animation de game over                    
                    play_loose();
                    game_lose = true;
                    if (bonus_active != null) {
                          clearInterval(bon);
                        }
                  } else {
                      if (collision_fruit()) {
                          exp = setInterval(function() { explosion(fruit.X + 10, fruit.Y + 10, 10)}, 50);
                          play_fruit();
                          length_min++;
                          points = points + 50; // on ajoute 50 points
                          document.querySelector("#points_value").textContent = points;
                          nb_fruit_get++;
                          document.querySelector("#fruits_value").textContent = nb_fruit_get;
                      } else {
                          if (collision_bonus()) {
                              clearInterval(bon);
                              erase_bonus(bonus_active.X, bonus_active.Y, rad);
                              bonus_active = null;
                              play_bonus_get();
                              points = points + (rad - 5) * 20; // on ajoute un nombre de points au prorata de la taille du bonus
                              document.querySelector("#points_value").textContent = points;
                              nb_bonus_get++;
                              document.querySelector("#bonus_value").textContent = nb_bonus_get;
                              rad = 20;
                          }    
                    }
                    draw_snake(); 
                  }
                break;  
            case 4 :  // left
                 if (snake[snake.length-1].X <= 0) { // le serpent atteint le limite gauche du canvas
                    add_snake(canvas_width -20 , snake[snake.length-1].Y);
                } else {
                    add_snake(snake[snake.length-1].X -20 , snake[snake.length-1].Y);
                }
                if (collision_tail()) { //le serpent se mord la queue
                    clearInterval(jeu);
                    clearInterval(temps_jeu);
                    game_over(); //animation de game over                    
                    play_loose();
                    game_lose = true;
                    if (bonus_active != null) {
                          clearInterval(bon);
                        }
                  } else { 
                    if (collision_fruit()) {
                          exp = setInterval(function() { explosion(fruit.X + 10, fruit.Y + 10, 10)}, 50);
                          play_fruit();
                          length_min++;
                          points = points + 50; // on ajoute 50 points
                          document.querySelector("#points_value").textContent = points;
                          nb_fruit_get++;
                          document.querySelector("#fruits_value").textContent = nb_fruit_get;
                    } else {
                        if (collision_bonus()) {
                            clearInterval(bon);
                            erase_bonus(bonus_active.X, bonus_active.Y, rad);
                            bonus_active = null;
                            play_bonus_get();
                            points = points + (rad - 5) * 20; // on ajoute un nombre de points au prorata de la taille du bonus
                            document.querySelector("#points_value").textContent = points;
                            nb_bonus_get++;
                            document.querySelector("#bonus_value").textContent = nb_bonus_get;
                            rad = 20;
                        }    
                    }
                    draw_snake(); 
                  }
                break;  
        }
        direction_blocked = false;
        log_key = log_key + "u";
    }
    
    // Affiche les coordonnées de tous les éléments du serpent pour le débugage
    function print_snake(){
        var phrase = "[";
        for (var i = 0; i < snake.length; i++) {
              phrase = phrase + "(" + snake[i].X + "," + snake[i].Y + "), ";
        }
        phrase = phrase + "]";
        if (fruit !== null) { // si le fruit est défini on l'affiche en débugage
            phrase = phrase + " , [(" + fruit.X + "," + fruit.Y + ")] , [Points : " + points + "]";
            document.querySelector("#affiche").textContent = phrase;
        }
    }
    
    //////////////////////// Fonctions pour les fruits /////////////////////
    
    var explose_state = 0;  // 0 : pas d'explosion de fruit en cours / 1 : explosion en cours
    var arcsector = Math.PI * (0.5); // la valeur de l'arc est d'un quart de cercle

    // draw_explose_right_top dessine un quart de cercle orienté Nord-Est
    function draw_explose_right_top(x_center, y_center, radius) {
        context.beginPath(); // haut droite
        context.moveTo(x_center, y_center);
        context.arc(x_center, y_center, radius, 3 * arcsector, 4 * arcsector, false);
        context.lineTo(x_center, y_center);
        context.fillStyle = '#6a5e6e';
        context.fill();
        context.strokeStyle = '#573563';
        context.stroke();
    }
    
    // draw_explose_right_top dessine un quart de cercle orienté Sud-Est
    function draw_explose_right_bottom(x_center, y_center, radius) {
        
        context.beginPath(); // bas droite
        context.moveTo(x_center, y_center);
        context.arc(x_center, y_center, radius, 0, arcsector, false);
        context.lineTo(x_center, y_center);
        context.fillStyle = '#6a5e6e';
        context.fill();
        context.strokeStyle = '#573563';
        context.stroke();
    }
    
    // draw_explose_right_top dessine un quart de cercle orienté Sud-Ouest
    function draw_explose_left_bottom(x_center, y_center, radius) {
        context.beginPath(); // bas gauche
        context.moveTo(x_center, y_center);
        context.arc(x_center, y_center, radius, arcsector, 2 * arcsector, false);
        context.lineTo(x_center, y_center);
        context.fillStyle = '#6a5e6e';
        context.fill();
        context.strokeStyle = '#573563';
        context.stroke();
    }

    // draw_explose_right_top dessine un quart de cercle orienté Nord-Ouest
    function draw_explose_left_top(x_center, y_center, radius) {
        context.beginPath(); // haut gauche
        context.moveTo(x_center, y_center);
        context.arc(x_center, y_center, radius, 2 * arcsector, 3 * arcsector, false);
        context.lineTo(x_center, y_center);
        context.fillStyle = '#6a5e6e';
        context.fill();
        context.strokeStyle = '#573563';
        context.stroke();
    }
    
    //draw_fruit dessine un cercle avec bordure
    function draw_fruit(x_center, y_center, radius) {
        context.beginPath();
        context.arc(x_center, y_center, 10, 0, 2 * Math.PI, false);
        context.fillStyle = '#6a5e6e';
        context.fill();
        context.lineWidth = 2;
        context.strokeStyle = '#573563';
        context.stroke();
    }
    
    function erase_explose(x_center, y_center, radius) {
        context.clearRect(x_center - radius - 1, y_center - radius - 1, radius * 2 + 2, radius * 2 + 2);    
    }
    
    //context.clearRect(139, 139, 22, 22);
    //context.clearRect(x - radius - 1, y - radius - 1, radius * 2 + 2, radius * 2 + 2);
    
    //explosion dessine 4 quarts de cercle en fonction de l'état explose_state de l'animation d'explosion
    function explosion(x_center, y_center, radius) {
         if (explose_state <= 6) {
             erase_explose(x_center, y_center, radius + explose_state * 2);
             draw_explose_right_top(x_center + explose_state * 2 + 2, y_center - explose_state * 2 - 2, radius - explose_state);
             draw_explose_right_bottom(x_center + explose_state * 2 + 2, y_center + explose_state * 2 + 2, radius - explose_state);
             draw_explose_left_bottom(x_center - explose_state * 2 - 2, y_center + explose_state * 2 + 2, radius - explose_state);
             draw_explose_left_top(x_center - explose_state * 2 - 2, y_center - explose_state * 2 - 2, radius - explose_state);                          
             explose_state++;
         } else {
             erase_explose(x_center, y_center, radius + explose_state * 2);
             explose_state = 0;
             clearInterval(exp);
             exp = null;
             fruit_appear();
         }
         draw_snake(); //on redessinne le serpent à chaque pas de l'explosion
    }
    
    //////////////////////// Fonction du bonus //////////////////////////////
    
    function erase_bonus(x_center, y_center, radius){
        context.clearRect(x_center + 10 - radius - 1, y_center + 10  - radius - 1, radius * 2 + 2, radius * 2 + 2);    
    }

    function bonus(x_center, y_center, radius) {
        if (radius != 5) {
            if ( ((radius >= 15) && (radius % 3 == 0)) || ((radius < 15) && (radius >= 10) && ((radius + 1) % 2 == 0)) || (radius < 10) ) {
                play_bonus_time();
            }
            context.beginPath();
            context.arc(x_center + 10, y_center + 10, radius, 0, 2 * Math.PI, false);
            if (rad % 2 == 0) {
                if (color == '#42faa1') {
                    color = '#ffa500';
                } else { 
                    color = '#42faa1';
                }
            }
            context.fillStyle = color;
            context.fill();
        } else {
            play_bonus_missed();
            clearInterval(bon);
            erase_bonus(x_center, y_center, radius);
            bonus_active = null;
            rad = 20;
            color = '#42faa1';
        }
    }
    
    function is_free_bonus(x_center, y_center, radius) { // renvoi true si la case est libre pour y placer un bonus
        var wrong_place_bonus = true; // le bonus apparait sur une case vide
        for (var i = 0; i < snake.length; i++) {
            if ((snake[i].X == x_center) && (snake[i].Y == y_center)) {
                wrong_place_bonus = false; // le bonus est sur le serpent
            }
        }
        if ((fruit.X == x_center) && (fruit.Y == y_center)){
            wrong_place_bonus = false; // le bonus apparait sur un fruit
        }
        if ((x_center == 0) || (x_center == 480) || (y_center == 0) || (y_center == 480)) {
            wrong_place_bonus = false; // le bonus est sur un bord (interdit pour des raisons esthetiques)
        }
        return wrong_place_bonus;
    }
    
    //////////////////////// Fonctions de jeu ///////////////////////////////
    
    // Joue le son de fin de partie
    function play_loose() {
        document.querySelector("#lose").play();
    }
    
    // Joue le son de gobage du fruit
    function play_fruit() {
        document.querySelector("#fruit").play();
    }
    
    // Joue le son de temps du bonus
    function play_bonus_time() {
        document.querySelector("#bonus_time").play();
    }
    
    // Joue le son du bonus raté
    function play_bonus_missed() {
        document.querySelector("#bonus_missed").play();
    }

    // Joue le son du bonus gobé
    function play_bonus_get() {
        document.querySelector("#bonus_get").play();
    }
    
    // Joue le son de la page de menu
    function play_menu() {
        document.querySelector("#menu").play();
    }
    
    // Detecte si la tête du serpent rentre en collision avec la queue du serpent (si elles ont les mêmes coordonnées)
    function collision_tail() {
            for (var i = 0; i < snake.length - 1; i++) {
                if ( (snake[i].X == snake[snake.length - 1].X) && (snake[i].Y == snake[snake.length - 1].Y) ){
                    return true;
                }
            }
                return false;
    }
    
    // Detecte si la tête du serpent rentre en collision avec un fruit
    function collision_fruit() {
            if ( (fruit.X == snake[snake.length - 1].X) && (fruit.Y == snake[snake.length - 1].Y) ){
                return true;
            } else {
                return false;    
            }
    }
    
    // Detecte si la tête du serpent rentre en collision avec un bonus
    function collision_bonus() {
        if (bonus_active != null) {        
            if ( (bonus_active.X == snake[snake.length - 1].X) && (bonus_active.Y == snake[snake.length - 1].Y) ){
                return true;
            } else {
                return false;    
            }
        } else {
            return false;
        }
    }
    
    // Fonction appelé lorsqu'une touche est appuyée
    function action(event){
        if ( (!direction_blocked) || (event.keyCode == 80) ) { //on empeche que la direction soit changé tant que le dernier interval n'est pas traité
            log_key = log_key + "b";
            direction_blocked = true;
            switch (event.keyCode) {
                case 38 :  // up arrow
                    if ( (direction != 3) && (pause != 1) ) {direction = 1; log_key = log_key + ", " + 1;}
                    break;  
                case 39 :  // right arrow
                    if ( (direction != 4) && (pause != 1) ) {direction = 2; log_key = log_key + ", " + 2;}
                    break;  
                case 40 :  // down arrow
                    if ( (direction != 1) && (pause != 1) ) {direction = 3; log_key = log_key + ", " + 3;}
                    break;  
                case 37 :  // left arrow
                    if ( (direction != 2) && (pause != 1) ) {direction = 4; log_key = log_key + ", " + 4;}
                    break;  
                case 80 : // p = pause
                    if (pause == 0) {
                        if (!game_lose) {
                            set_pause(); // on affiche l'écran de pause
                        }
                        clearInterval(jeu);
                        clearInterval(temps_jeu);
                        if (bonus_active != null) {
                          clearInterval(bon);
                        }
                        pause = 1;
                    } else {
                        if (!game_lose) { //si la partie est perdue on ne relance pas les timers
                            context.clearRect(0, 0, 500, 500); // on efface l'écran de pause
                            jeu = setInterval(go_snake, speed_snake);
                            temps_jeu = setInterval(write_time,1000);
                            if (bonus_active != null){
                                bon = setInterval(function() {erase_bonus(bonus_active.X, bonus_active.Y, rad + 1); bonus(bonus_active.X, bonus_active.Y, rad); rad = rad -1; draw_snake();}, 250);
                            }
                            pause = 0;
                        }
                    }
                    log_key = log_key + ", P";
                default : 
                    break;
            }
        }
       //document.querySelector("#keys").textContent = log_key; //affiche les logs de touches préssées et d'interval
    }
    
    // fonction faisant apparaitre un fruit
    function fruit_appear() { // le fuit est composé d'un item pour gérer la collision
        var in_snake = false; //le fruit n'apparait pas sur le serpent / true si le fruit est généré sur le serpent
        fruit = new Item(Math.floor(Math.random() * canvas_width/20)*20, Math.floor(Math.random() * canvas_width/20)*20);
        for (var i = 0; i < snake.length; i++) {
            if ((snake[i].X == fruit.X) && (snake[i].Y == fruit.Y)) {
                in_snake = true;
            }
        }
        if (in_snake) {
            fruit_appear();
        } else {
            nb_fruit++;
            draw_fruit(fruit.X +10, fruit.Y +10);    // mais au niveau de l'affichage on lui passe son centre en paramètre
        }
    }
    
    function write_time() { //affiche le temps de jeu dans le bandeau supérieur
        time_second++;
        var seconde = time_second % 60;
        var minute = (time_second - seconde)/60;
        document.querySelector("#temps_value").textContent = minute+"min " + seconde + "s";
    }
    
    function set_pause() {
        context.fillStyle = "rgba(0, 0, 0, 0.5)";
        context.fillRect(0, 0, 500, 500); 
        context.fillStyle = "#FFCDE1";
        context.fillRect(175, 225, 150, 50);  
        context.font = "2rem Comic Sans MS";
        context.fillStyle = "rgb(0,0,0)";
        context.fillText("Pause", 210, 260);
    }
    
    function game_over() { //animation de fin de partie
        var blink = 0;
        var blink_timer = setInterval(function() {
                                   if (blink < 1000) { //animation durant 1 secondes
                                       if (blink % 400 == 0) {
                                           document.querySelector("#jeu").style.backgroundColor ="#FEF65B";    
                                       } else {
                                           document.querySelector("#jeu").style.backgroundColor ="#FFFF99";    
                                       }
                                   } else { // et affichage de l'écran game over
                                       context.fillStyle = "rgba(74, 83, 84, 0.75)";
                                       context.fillRect(0, 0, 500, 500); 
                                       context.fillStyle = "#0ABAB5";
                                       context.fillRect(165, 125, 170, 50); 
                                       context.fillRect(165, 175, 170, 50); 
                                       context.font = "2rem Comic Sans MS";
                                       context.fillStyle = "rgb(0,0,0)";
                                       context.fillText("Game Over", 167, 160);
                                       context.fillStyle = "#E02B81";
                                       if (points < 100) {
                                           context.fillText("00" + points, 175, 210);
                                       } else {
                                           context.fillText(points, 175, 210);
                                       }
                                       context.fillStyle = "rgb(0,0,0)";
                                       context.fillText("pts", 260, 210);
                                       context.fillStyle = "#0ABAB5";
                                       context.fillRect(165, 225, 170, 50); 
                                       context.fillStyle = "rgb(0,0,0)";
                                       context.fillText("Rejouer", 190, 260);
                                       clearInterval(blink_timer);
                                       
                                       context.fillStyle = "#4FCB5B";
                                       context.fillRect(375, 430, 120, 60);
                                       context.font = "bold 2rem Comic Sans MS";
                                       context.fillStyle = "rgb(0,0,0)";
                                       context.fillText("Menu", 395, 470);
                                       
                                       if (bigger_scores()) {
                                           var pseudo_field = document.createElement("input");
                                           pseudo_field.setAttribute('id', 'pseudo');
                                           pseudo_field.setAttribute('type', 'text');
                                           pseudo_field.setAttribute('placeholder', 'Votre pseudo');
                                           document.querySelector("body").appendChild(pseudo_field);
                                           context.fillStyle = "#0ABAB5";
                                           context.fillRect(165, 330, 170, 50); 
                                           context.fillRect(165, 380, 170, 30);
                                           context.font = "1.2rem Comic Sans MS";
                                           context.fillStyle = "rgb(0,0,0)";
                                           context.fillText("Enregistrer score", 170, 400);
                                       }
                                   }
                                   blink+= 200;
                                },200);
    }
    
    function erase_canvas() {
        context.clearRect(0, 0, 500, 500);        
        if (document.querySelector("#pseudo") != null) {
            document.querySelector("body").removeChild(document.querySelector("#pseudo"));
        }
        if (document.querySelector("#liste_scores") != null) {
            document.querySelector("body").removeChild(document.querySelector("#liste_scores"));
        }
    }
    
    function mouse_move(event) { // lorsque l'on survol le canvas avec la souris
        var x = event.clientX;
        var y = event.clientY;
        if (game_lose) {
            if ((x >= 173) && (x <= 343) && (y >= 285) && (y <= 335) && !menu_page && !scores_page){
                this.style.cursor="pointer";
                context.fillStyle = "#FFF1AD";
                context.fillRect(165, 225, 170, 50); 
                context.font = "2rem Comic Sans MS";
                context.fillStyle = "rgb(0,0,0)";
                context.fillText("Rejouer", 190, 260);
            } else {
                if (!menu_page && !scores_page) {
                    this.style.cursor="default";
                    context.fillStyle = "#0ABAB5";
                    context.fillRect(165, 225, 170, 50); 
                    context.font = "2rem Comic Sans MS";
                    context.fillStyle = "rgb(0,0,0)";
                    context.fillText("Rejouer", 190, 260);
                    if ((x >= 173) && (x <= 343) && (y >= 440) && (y <= 490) && bigger_scores()){  // champs et bouton d'enregistrement du score
                        this.style.cursor="pointer";
                        context.fillStyle = "#FFF1AD";
                        context.fillRect(165, 380, 170, 30);
                        context.font = "1.2rem Comic Sans MS";
                        context.fillStyle = "rgb(0,0,0)";
                        context.fillText("Enregistrer score", 170, 400);
                    } else { 
                        if (bigger_scores()) {
                            this.style.cursor="default";
                            context.fillStyle = "#0ABAB5";
                            context.fillRect(165, 380, 170, 30);
                            context.font = "1.2rem Comic Sans MS";
                            context.fillStyle = "rgb(0,0,0)";
                            context.fillText("Enregistrer score", 170, 400);
                        }
                        if ((x >= 383) && (x <= 503) && (y >= 491) && (y <= 551)){ // le joueur survol le bouton de menu de l'écran de game over
                            this.style.cursor="pointer";
                            context.fillStyle = "#EC9D72";
                            context.fillRect(375, 430, 120, 60);
                            context.font = "bold 2rem Comic Sans MS";
                            context.fillStyle = "rgb(0,0,0)";
                            context.fillText("Menu", 395, 470);                
                        } else {
                            this.style.cursor="default";
                            context.fillStyle = "#4FCB5B";
                            context.fillRect(375, 430, 120, 60);
                            context.font = "bold 2rem Comic Sans MS";
                            context.fillStyle = "rgb(0,0,0)";
                            context.fillText("Menu", 395, 470);
                        }
                    }
                }
            }
        }
        if (menu_page && !scores_page) { // champs et bouton de la page de menu
            if ((x >= 48) && (x <= 238) && (y >= 471) && (y <= 531)){  // champs et bouton de lancement d'une partie
                this.style.cursor="pointer";
                context.fillStyle = "#EC9D72";
                context.fillRect(40, 410, 190, 60);
                context.font = "bold 2rem Comic Sans MS";
                context.fillStyle = "rgb(0,0,0)";
                context.fillText("Jouer", 90, 450);
            } else {
                this.style.cursor="default";
                context.fillStyle = "#4FCB5B";
                context.fillRect(40, 410, 190, 60);
                context.font = "bold 2rem Comic Sans MS";
                context.fillStyle = "rgb(0,0,0)";
                context.fillText("Jouer", 90, 450);
                if ((x >= 278) && (x <= 468) && (y >= 471) && (y <= 531)){    // champs et bouton d'accès à la page des scores
                    this.style.cursor="pointer";
                    context.fillStyle = "#EC9D72";
                    context.fillRect(270, 410, 190, 60);
                    context.font = "bold 2rem Comic Sans MS";
                    context.fillStyle = "rgb(0,0,0)";
                    context.fillText("Scores", 315, 450);
                } else {
                    this.style.cursor="default";
                    context.fillStyle = "#4FCB5B";
                    context.fillRect(270, 410, 190, 60);
                    context.font = "bold 2rem Comic Sans MS";
                    context.fillStyle = "rgb(0,0,0)";
                    context.fillText("Scores", 315, 450);
                }
                }        
        }
        
        if (scores_page && !menu_page) {
            if ((x >= 383) && (x <= 503) && (y >= 491) && (y <= 551)){ // le joueur survol le bouton menu de l'écran des scores
                this.style.cursor="pointer";
                context.fillStyle = "#EC9D72";
                context.fillRect(375, 430, 120, 60);
                context.font = "bold 2rem Comic Sans MS";
                context.fillStyle = "rgb(0,0,0)";
                context.fillText("Menu", 395, 470);                
            } else {
                this.style.cursor="default";
                context.fillStyle = "#4FCB5B";
                context.fillRect(375, 430, 120, 60);
                context.font = "bold 2rem Comic Sans MS";
                context.fillStyle = "rgb(0,0,0)";
                context.fillText("Menu", 395, 470);
            }
        }
    }
    
    function mouse_click(event) { // lorsque l'on clique sur le canvas
        var x = event.clientX;
        var y = event.clientY;
        if (game_lose && !menu_page && !scores_page) {
            if ((x >= 173) && (x <= 343) && (y >= 285) && (y <= 335)){ // le joueur clique sur rejouer
                this.style.cursor="default";
                play();
            }
            if ((x >= 173) && (x <= 343) && (y >= 440) && (y <= 490)){ // le joueur clique sur enregistrer résultat
                var pseudo = document.querySelector("#pseudo").value;
                if (pseudo != "") { // si le joueur a saisi un pseudo on envoi le résultat au serveur
                    var request = new XMLHttpRequest();
                    request.open('POST', 'http://localhost/snake/classement.php');
                    var resultat = '{"pseudo": "' + pseudo + '", "score" : ' + points +', "temps" : ' + time_second + ', "fruits" : ' + nb_fruit_get + ', "bonus" : ' + nb_bonus_get + '}';
                    request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                    request.send("envoi=" + resultat); // envoi de la requete au serveur pour enregistrement du score
                    this.style.cursor="default";
                    score();
                                                
                    
                }
            }
        }
        if (menu_page && !scores_page) { // gestion des évenements click de la page de menu
            if ((x >= 48) && (x <= 238) && (y >= 471) && (y <= 531)){ // le joueur clique sur jouer
                // lancement d'une partie
                this.style.cursor="default";
                document.querySelector("#menu").pause();
                play();
            } else {
                if ((x >= 278) && (x <= 568) && (y >= 471) && (y <= 531)){  // le joueur clique sur scores
                    //accès à la page des scores
                    document.querySelector("#menu").pause();
                    this.style.cursor="default";
                    score();
                }
            }
        } else { // gestion des evenements click de la page des scores
            if ((scores_page && !menu_page) || (game_lose && !menu_page)) {
                if ((x >= 383) && (x <= 503) && (y >= 491) && (y <= 551)){ // le joueur sur le retour au menu à partir de la page scores
                    // retour au menu
                    this.style.cursor="default";
                    menu();
                }
            }
        }
 
    }
    
    function menu() {
        menu_page = true;
        scores_page = false;
        erase_canvas(); // on réinitialise l'affichage
        
        document.getElementById("points_value").textContent = "0";
        document.getElementById("temps_value").textContent = "0m 00s";
        document.getElementById("fruits_value").textContent = "0";
        document.getElementById("bonus_value").textContent = "0";        
        play_menu();
        recupere_scores(); // on récupère le tableau des résultats
        context.fillStyle = "#95D8DF";
        context.fillRect(0, 0, 500, 375); 
        context.fillStyle = "#F8E50F";
        context.fillRect(0, 375, 500, 125); 
        context.font = "bold 5.5rem Comic Sans MS";
        context.fillStyle = "rgb(0,0,0)";
        context.fillText("Snake", 125, 100);
        var image = new Image();
        image.src = "snake.png"
        image.onload = function () {
            context.drawImage(image, 125, 120);
        }
        context.fillStyle = "#4FCB5B";
        context.fillRect(40, 410, 190, 60); 
        context.fillRect(270, 410, 190, 60);
        context.font = "bold 2rem Comic Sans MS";
        context.fillStyle = "rgb(0,0,0)";
        context.fillText("Jouer", 90, 450);
        context.fillText("Scores", 315, 450);
    }
    
    function recupere_scores() {
        request_send_score = true; // la requete est en cours d'envoi
        var request = new XMLHttpRequest();
        request.open('GET', 'http://localhost/snake/classement.php');
        request.onload = function () {  // chargement des parties existantes
            if (request.status >= 200 && request.status < 400) {
                scores = JSON.parse(request.responseText);                
                request_send_score = false; // la requete est terminée
            }
        }
        request.send();
    }
    
    function bigger_scores() { //renvoi true si le scores en cours est plus grand que l'un des scores enregistrés ou si il y a moins de 15 scores enregistrés
        var bigger = false;
        if (scores.infos.resultats < 15) {
            bigger = true;
        } else {
            for(var i = 0; i < scores.infos.resultats; i++) {
                if (points > scores.resultats[i].score) {
                    bigger = true;
                }
            }
        }
        return bigger;
    }
    
    function score() { // affiche l'écran des scores
        scores_page = true;
        menu_page = false;
        
        
        //on récupère à nouveau les scores pour la fin de partie
        recupere_scores();
        var wait_scores = setInterval(function() {
            if (!request_send_score){ // on attend la fin de la requete AJAX de recupere_scores avant d'afficher les résultats
        
                // Réinitialisation du plateau de jeu
                erase_canvas();
                document.querySelector("#jeu").style.backgroundColor ="#DAD0D2"; // on réinitialise la couleur du canvas
        
                var tableau_score = [];
        
                for(var i = 0; i < scores.infos.resultats; i++) {
                    tableau_score.push(scores.resultats[i]);
            }
        
            tableau_score.sort(function(a, b){return b.score-a.score}); // on tri le tableau des scores en fonction de l'attribut score
            var liste_scores = document.createElement("table"); // les scores sont affichés dans un tableau
            liste_scores.setAttribute("id", "liste_scores");
            var tr = document.createElement("tr"); // on définit d'abord les titres du tableau
            tr.setAttribute("id", "tr_titre");
            var td = document.createElement("td");
            td.setAttribute("id", "td_titre");
            td.textContent = "Rang";
            tr.appendChild(td);
            td = document.createElement("td");
            td.setAttribute("id", "td_titre");
            td.textContent = "Nom";
            tr.appendChild(td);
            td = document.createElement("td");
            td.setAttribute("id", "td_titre");
            td.textContent = "Points";
            tr.appendChild(td);
            td = document.createElement("td");
            td.setAttribute("id", "td_titre");
            td.textContent = "Temps";
            tr.appendChild(td);
            td = document.createElement("td");
            td.setAttribute("id", "td_titre");
            td.textContent = "Fruits";
            tr.appendChild(td);
            td = document.createElement("td");
            td.setAttribute("id", "td_titre");
            td.textContent = "Bonus";
            tr.appendChild(td); 
            liste_scores.appendChild(tr);
            for(var i = 0; i < tableau_score.length; i++) { // pour chaque résultat on crée une nouvelle ligne du tableau
                tr = document.createElement("tr");
                tr.setAttribute("id", "tr_score");
                td = document.createElement("td");
                td.setAttribute("id", "td_score_rang"); 
                td.textContent = i+1;
                tr.appendChild(td);
                td = document.createElement("td");
                td.setAttribute("id", "td_score_nom");
                td.textContent = tableau_score[i].pseudo;
                tr.appendChild(td);
                td = document.createElement("td");
                td.setAttribute("id", "td_score_points");
                td.textContent = tableau_score[i].score;
                tr.appendChild(td);
                td = document.createElement("td");
                td.setAttribute("id", "td_score_temps");
                var seconde = tableau_score[i].temps % 60;
                var minute = (tableau_score[i].temps - seconde)/60;
                td.textContent = minute+"min " + seconde + "s";
                tr.appendChild(td);
                td = document.createElement("td");
                td.setAttribute("id", "td_score_fruits");
                td.textContent = tableau_score[i].fruits;
                tr.appendChild(td);
                td = document.createElement("td");
                td.setAttribute("id", "td_score_bonus");
                td.textContent = tableau_score[i].bonus;
                tr.appendChild(td); 
                liste_scores.appendChild(tr);
            }
        
            document.querySelector("body").appendChild(liste_scores);
        
            context.fillStyle = "#4FCB5B";
            context.fillRect(375, 430, 120, 60);
            context.font = "bold 2rem Comic Sans MS";
            context.fillStyle = "rgb(0,0,0)";
            context.fillText("Menu", 395, 470);
            clearInterval(wait_scores); // on arrète le timer
        }},1);

    }
    
    function play() { // lancement d'une partie
        
        // Réinitialisation de variables
        snake = [];
        scores = []; // on initialise le tableau des résultats
        direction = 2; // 1 : top; 2 : right; 3 : bottom; 4 : left;
        length_min = 12;
        speed_snake = 100; //rafraichissement toutes les 100ms
        fruit = null; // un fruit est toujours présent du le terrain de jeu
        nb_fruit = 0; // nombre de fruits apparus durant la partie
        nb_fruit_get = 0; //nombre de fruits gobés durant la partie
        exp = null; // timer d'explosion d'un fruit
        bonus_active = null; // si un bonus est présent sur le terrain
        nb_bonus = 0; // nombre de bonus apparu dans le jeu
        nb_bonus_get = 0; // nombre de bonus gobés dans le jeu
        interval_bonus = 5; //nombre de fruits nécessaires pour l'apparition d'un bonus
        color = '#42faa1'; // couleur de départ du bonus
        rad = 20; // rayon du bonus
        bon = null; // timer du bonus
        points = 0; // on initialise les points à 0
        pause = 0; //0 -> le jeu est en cours / 1 -> le jeu est en pause
        direction_blocked = false; //true : le serpent est en cours de déplacement / pour éviter les saisies multiples pendant un interval
        log_key = "keys : "; // stocke les touches préssées
        game_lose = false; // true lorsque la partie est perdue
        temps_jeu = null; // timer pour compter les secondes écoulées
        time_second = 0;    
        document.querySelector("#jeu").style.backgroundColor ="#FFFF99"; // on réinitialise la couleur du canvas
        menu_page = false; //le jeu commence nous avons quitté le menu
        scores_page = false; // true si l'on se trouve à la page des scores
        request_send_score = false; // la requete de récupération des scores n'est pas en cours d'envoi
        
        //on récupère à nouveau les scores pour la fin de partie
        recupere_scores();
        
        // Réinitialisation du plateau de jeu
        erase_canvas();
        
        // Initialisation du jeu

        // position de départ du serpent
        add_snake(60, 140);    
        add_snake(80, 140);    
        add_snake(100, 140);    
        add_snake(120, 140);    
        add_snake(140, 140);    
        add_snake(160, 140);    
        add_snake(180, 140);    
        add_snake(200, 140);    
        add_snake(220, 140);    
        add_snake(240, 140);
        add_snake(260, 140);
        add_snake(280, 140);

        // apparition du fruit
        fruit_appear();
        
        // apparition du serpent
        draw_snake();
        
        jeu = setInterval(go_snake, speed_snake);
    }
        
    menu();
    document.addEventListener("keydown", action);
    canvas.addEventListener("click", mouse_click);
    canvas.addEventListener("mousemove", mouse_move);
    
}
